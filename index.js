console.log('Hello, World')
// Details
const details = {
    fName: "Nicholas John",
    lName: "Libornio",
    age: 17,
    hobbies : [
        "basketball", "singing", "listening to music"
    ] ,
    workAddress: {
        housenumber: "Blk 61, Lot 22",
        street: "Villa Luisa North",
        city: "Caloocan City",
        state: "Philippines",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");